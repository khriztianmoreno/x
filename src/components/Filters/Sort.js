import React, {Component} from "react";
import {
  View,
  H1,
  Text,
  InputGroup,
  Input,
  Icon,
  Header,
  Container,
  Button,
  List,
  ListItem,
  Badge,
  Content,
  Card,
  CardItem,
  Left,
  Right,
  Title,
  Body,
  Grid,
  Col,
  Radio
} from "native-base";
import {
  Image,
  Dimensions,
  Styleheet,
  TouchableOpacity,
  ScrollView,
  Platform
} from "react-native";
import RadioButton from 'react-native-radio-button';

import MyFooter from "../CommonComponents/Footer";
import ThemeHeader from "../CommonComponents/Header/index.js";

import Style from "./style.js";

const deviceWidth = Dimensions
  .get("window")
  .width;

export default class Sort extends Component {
  constructor(props) {
    super(props);
    const {params} = props.navigation.state;

    this.state = {};
  }

  render() {
    const navigation = this.props.navigation;
    const filters = [
      {
        id: 1,
        name: "Featured",
        isSelected: true,
      }, {
        id: 2,
        name: "Newest",
        isSelected: false,
      }, {
        id: 3,
        name: "Price High to Low",
        isSelected: true,
      }, {
        id: 4,
        name: "Price Low to High",
        isSelected: false,
      }, {
        id: 5,
        name: "Sale",
        isSelected: false,
      }, {
        id: 6,
        name: "Raiting",
        isSelected: true,
      }
    ];

    return (
      <Container>
        <Content
          showsVerticalScrollIndicator={false}
          style={{
          marginBottom: 0,
          flex: 1
        }}>
          <View
            style={{
            alignItems: "center",
            height: 50,
            width: deviceWidth,
            backgroundColor: "#fff",
            flexDirection: "row",
            borderBottomColor: "#ddd",
            borderBottomWidth: 1
          }}>
            <View
              style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: "space-between",
              alignItems: "center",
              paddingTop: 20,
            }}>
              <Button transparent>
                <Icon
                  onPress={() => navigation.goBack()}
                  style={{padding: 5, marginLeft: 5}}
                  name="arrow-back"/>
              </Button>
              <Text style={{alignItems: "center"}}>
                SORT
              </Text>
              <Button transparent onPress={() => navigation.goBack()}>
                <Text style={{alignItems: "center", color: "#6363e8"}}>
                  Apply
                </Text>
              </Button>
            </View>
          </View>
          <Card>
            <CardItem style={{ padding: 0 }}>
              <List
                removeClippedSubviews={false}
                bounces={false}
                dataArray={filters}
                renderRow={list => <ListItem
                icon
                style={Style.listDropItems}
                onPress={() => navigation.navigate("FilterSort")}>
                <Body>
                  <Text style={Style.listDropText}>
                    {list.name}
                  </Text>
                </Body>
                <Right>
                  <RadioButton
                    animation={'bounceIn'}
                    isSelected={list.isSelected}
                    innerColor={'#6363e8'}
                    outerColor={'#6363e8'}
                    onPress={() => {list.isSelected = !list.isSelected}}
                  />
                </Right>
              </ListItem>}/>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
