import React, {Component} from "react";
import {
  View,
  H1,
  Text,
  InputGroup,
  Input,
  Icon,
  Header,
  Container,
  Button,
  List,
  ListItem,
  Badge,
  Content,
  Card,
  CardItem,
  Left,
  Right,
  Title,
  Body,
  Grid,
  Col
} from "native-base";
import {
  Image,
  Dimensions,
  Styleheet,
  TouchableOpacity,
  ScrollView,
  Platform
} from "react-native";

import MyFooter from "../CommonComponents/Footer";
import ThemeHeader from "../CommonComponents/Header/index.js";

import Style from "./style.js";

const deviceWidth = Dimensions.get("window").width;


class Category extends Component {
  constructor(props) {
    super(props);
    const { params } = props.navigation.state;

    this.state = {
      listCategories: params ? params.list : [],
      listName: params ? params.name : null,
      categoryDrop: null
    };
  }

  render() {
    const navigation = this.props.navigation;
    const filters = [
        {
            id: 1,
            name: "SORT",
            navigate: "FilterSort",
            selected: "Featured",
        },
        {
            id: 2,
            name: "SIZE",
            navigate: "FilterSize",
            selected: "Any",
        },
        {
            id: 3,
            name: "COLOR",
            navigate: "FilterColor",
            selected: "Any",
        },
        {
            id: 4,
            name: "PRICE",
            navigate: "FilterSort",
            selected: "Any",
        },
        {
            id: 5,
            name: "BRAND",
            navigate: "FilterSort",
            selected: "Any",
        },
        {
            id: 6,
            name: "DRESS LENGTH",
            navigate: "FilterSort",
            selected: "Any",
        },
        {
            id: 7,
            name: "DRESS STYLE",
            navigate: "FilterSort",
            selected: "Any",
        }
    ];

    return (
      <Container>
        <ThemeHeader PageTitle="Filters" IconRight="search" searchBar="true" navigation={navigation} />
        <Content
          showsVerticalScrollIndicator={false}
          style={{
          marginBottom: 0,
          flex: 1
        }}>
          <View
            style={{
            alignItems: "center",
            height: 50,
            width: deviceWidth,
            backgroundColor: "#fff",
            flexDirection: "row",
            borderBottomColor: "#ddd",
            borderBottomWidth: 1
          }}>
            <View
              style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: "space-between",
              alignItems: "center"
            }}>
              <Button transparent>
                <Icon
                  onPress={() => navigation.goBack()}
                  style={{padding: 5, marginLeft: -5}}
                  name="arrow-back"/>
              </Button>
              <Text style={{alignItems: "center"}}>
                Filters
              </Text>
              <Button transparent></Button>
            </View>
          </View>
          <Card>
            <CardItem style={{padding: 0}}>
              <List
                removeClippedSubviews={false}
                bounces={false}
                dataArray={filters}
                renderRow={list => <ListItem
                icon
                style={Style.listDropItems}
                onPress={() => navigation.navigate(list.navigate)}>
                <Body style={{justifyContent: "space-between", flexDirection: 'row'}}>
                  <Text style={Style.listDropText}>
                    {list.name}
                  </Text>
                  <Text style={Style.listDropSubText}>
                    {list.selected}
                  </Text>
                </Body>
                <Right>
                  <Icon style={Style.listIcon} name="ios-arrow-forward"/>
                </Right>
              </ListItem>}/>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
export default Category;
