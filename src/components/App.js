import React from "react";
import { StackNavigator } from "react-navigation";
import { Root } from "native-base";

import ProductList from "./ProductList";
import ProductPage from "./ProductPage";
import CATEGORIES from "./Category";
import ScanCamera from "./ScanCamera"
// import UploadImages from "./ScanCamera/UploadImage"
import Refine from './CommonComponents/SortRefine/Refine'
import Filters from './Filters'
import Sort from './Filters/Sort.js'
import Size from './Filters/Size.js'
import Color from './Filters/Color.js'

const Main = StackNavigator(
  {
    CATEGORIES: { screen: CATEGORIES },
    ProductList: { screen: ProductList },
    ProductPage: { screen: ProductPage },
    ScanCamera: { screen: ScanCamera },
    // UploadImages: { screen: UploadImages },
    Refine: { screen: Refine },
    Filters: { screen: Filters },
    FilterSort: { screen: Sort },
    FilterSize: { screen: Size },
    FilterColor: { screen: Color },
  },
  {
    index: 0,
    initialRouteName: "CATEGORIES",
    headerMode: "none"
  }
);

export default () => (
  <Root>
    <Main />
  </Root>
)
