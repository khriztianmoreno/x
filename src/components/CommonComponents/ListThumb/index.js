import React, { Component } from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import {
  Text,
  Card,
  CardItem,
  Button,
  Icon
} from 'native-base';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

import IconMI from 'react-native-vector-icons/MaterialIcons';

class ListThumb extends Component {
  render() {
    const navigation = this.props.navigation;
    // const imageSource = require("../../../assets/images/saleThumb1.png");
    const thumbWidth = deviceWidth/2;
    const thumbHeight = (deviceHeight/2)-5;
    return (
      <View style={{height: thumbHeight, width: thumbWidth}}>
        <Card style={{ shadowOpacity: 0, margin:0}}>
          <CardItem button onPress={() => navigation.navigate("ProductPage", { id: this.props.id })} style={{flex: 1, flexDirection: 'column',padding:0}}>
            <Image style={{flex: 3, resizeMode: 'contain', height:150, width: 150}} source={{ uri: this.props.imageSource }}/>
            <View style={{flex: 1, flexDirection: 'row', paddingLeft: 10,  width: thumbWidth}}>
              <View>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', width: thumbWidth, marginTop: 10}}>
                  <Text style={{fontSize: 13, fontWeight: '300', color: '#555'}}>{this.props.brand}</Text>
                </View>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={{fontSize: 11, fontWeight: '300', color: '#888', marginBottom: 4}} numberOfLines={1} ellipsizeMode='tail'>
                    {this.props.description}
                  </Text>
                </View>
                <View style={{flex: 1, flexDirection: 'row' , justifyContent: 'center'}}>
                  <Text style={{fontSize: 13, fontWeight: '500', color: '#555'}}>{this.props.discountedPrice}</Text>
                  <Text style={{fontSize: 11, fontWeight: '300', color: '#888', marginLeft: 2, textDecorationLine: 'line-through'}}>{this.props.price}</Text>
                </View>
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}
export default ListThumb;
