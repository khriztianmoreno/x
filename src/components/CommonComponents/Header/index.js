import React, { Component } from "react";
import { Image, Dimensions, TouchableOpacity, StyleSheet } from "react-native";
import {
  Container,
  Header,
  View,
  Title,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  StyleProvider,
  Item,
  Input
} from "native-base";
import { NavigationActions } from 'react-navigation'

import getTheme from "../../../theme/components/index";
import commonColor from "../../../theme/variables/commonColor";

import Style from "./style.js";

const navigateAction = NavigationActions.navigate({
  routeName: 'ScanCamera'
})


class ThemeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const navigation = this.props.navigation;

    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Header searchBar rounded>
          <Left>
            <Button transparent>
            {
              this.props.IconLeft ? (
                <Icon
                  onPress={() => navigation.goBack()}
                  style={{ padding: 5, marginLeft: -5 }}
                  name={this.props.IconLeft}
                />
              ) : (
                <Image source={require('./../../../images/icons/catalog.png')} />
              )
            }
            </Button>
          </Left>
          <Body>
            {
              this.props.searchBar ? (
                <Item style={{borderBottomColor: "#ddd", borderBottomWidth: 1}}>
                  <Icon name="ios-search" />
                  <Input placeholder="Search in catalog" style={{ color: "#555", fontSize: 16, fontWeight: "400" }} />
                </Item>
              ) : (
                <Title style={{ color: "#555", fontSize: 16, fontWeight: "400" }}>
                  {this.props.PageTitle}
                </Title>
              )
            }
          </Body>
          <Right>
            {this.props.IconRight &&
              <Button transparent onPress={() => navigation.dispatch(navigateAction)}>
                <Image source={require('./../../../images/icons/scan-code.png')} />
              </Button>}
          </Right>
        </Header>
      </StyleProvider>
    );
  }
}
export default ThemeHeader;
