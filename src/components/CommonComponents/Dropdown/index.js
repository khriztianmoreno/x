import React, { Component } from "react";
import { View, Image, Dimensions, Modal, TouchableOpacity } from "react-native";
import { Text, Card, CardItem, Icon } from "native-base";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  items(list) {
    return list.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          style={{ width: deviceWidth, backgroundColor: 'white' }}
          onPress={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <View style={{ alignItems: "center" }}>
            {
              item.selected ? (
                <Text
                  style={{
                    fontSize: 15,
                    padding: 12,
                    fontWeight: "500",
                    color: "#7468C5",
                    textAlign: "center",
                  }}
                >
                  {item.name}
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 15,
                    padding: 12,
                    fontWeight: "500",
                    color: "#777",
                    textAlign: "center"
                  }}
                >
                  {item.name}
                </Text>
              )
            }
          </View>
        </TouchableOpacity>
      )
    })
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          width: deviceWidth / 2,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert("Modal has been closed.");
          }}
        >
          <View
            style={{
              height: deviceHeight,
              width: deviceWidth,
              alignItems: "center",
              justifyContent: "flex-end",
              marginTop: 40
            }}
          >
            <TouchableOpacity
              style={{ height: deviceHeight / 2 + 20, width: deviceWidth, backgroundColor: 'transparent', }}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            >
              <View />
            </TouchableOpacity>
            <View
              style={{
                height: deviceHeight / 2 + 20,
                width: deviceWidth,
                backgroundColor: 'white',
                alignItems: "center",
                borderTopColor: "#333",
                borderTopWidth: 0.5,
                shadowColor: "#777",
                shadowOpacity: 0.2,
                shadowRadius: 1
              }}
            >
              <View
                style={{
                  width: deviceWidth,
                  backgroundColor: 'white',
                  borderBottomColor: "#ddd",
                  borderBottomWidth: 1,
                  padding: 10,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Text  style={{ fontSize: 16, fontWeight: "300", color: "#808090", textAlign: "center" }}>
                  Cancel
                </Text>
                <Text  style={{ fontSize: 17, fontWeight: "300", color: "#808090", textAlign: "center" }}>
                  Size
                </Text>
                <Text  style={{ fontSize: 16, fontWeight: "600", color: "#6363e8", textAlign: "center" }}>
                  Select
                </Text>
              </View>

              {this.items(this.props.dataList)}
            </View>
          </View>
        </Modal>
        <TouchableOpacity
          onPress={() => {
            this.setModalVisible(true);
          }}
        >
          <View
            style={{
              height: deviceHeight / 13,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Icon
              style={{ color: "#555", fontWeight: "300", fontSize: 24 }}
              name="ios-arrow-down"
            />
            <Text
              style={{
                color: "#555",
                fontWeight: "300",
                marginLeft: 8,
                paddingBottom: 2,
                fontSize: 12
              }}
            >
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Dropdown;
