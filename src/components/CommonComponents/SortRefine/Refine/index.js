import React, { Component } from "react";
import {
  View,
  Image,
  Dimensions,
  Modal,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import {
  Text,
  Card,
  CardItem,
  Icon,
  Header,
  Button,
  Title,
  Footer,
  Container,
  Content,
  List,
  Tabs
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

import ThemeHeader from "../../Header/index.js";
import Brand from "./Brand.js";
import Color from "./Color.js";
import Price from "./Price.js";
import Discount from "./Discount.js";
import CommonRefine from "./CommonRefine.js";
import commonColor from "../../../../theme/variables/commonColor.js";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

class Refine extends Component {
  state = {
    component: "Brand",
    modalVisible: true,
    color: "#000"
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    const dataSaleThumb = [
      {
        id: 1,
        categories: "Brand"
      },
      {
        id: 2,
        categories: "Colour"
      },
      {
        id: 3,
        categories: "Price"
      },
      {
        id: 4,
        categories: "Discount"
      },
      {
        id: 5,
        categories: "Sizes"
      },
      {
        id: 6,
        categories: "Pattern"
      },
      {
        id: 7,
        categories: "Collar"
      },
      {
        id: 8,
        categories: "Fabric"
      },
      {
        id: 9,
        categories: "Set Contents"
      },
      {
        id: 10,
        categories: "Fit"
      },
      {
        id: 11,
        categories: "Occasion"
      }
    ];

    const navigation = this.props.navigation;
    
    return (
      <Container>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Modal
            animationType={"fade"}
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              alert("Modal has been closed.");
            }}
          >
            <View
              style={{
                flexDirection: "row",
                paddingHorizontal: 6,
                paddingTop: 12,
                paddingBottom: 0,
                justifyContent: "space-between",
                backgroundColor: "#F9F9F9",
                borderBottomColor: "#aaa",
                borderBottomWidth: 0.5
              }}
            >
              <Button
                transparent
                onPress={() => navigation.goBack()}
              >
                <Icon
                  style={{ padding: 5, marginLeft: -5 }}
                  name="arrow-back"
                />
              </Button>
              <View
                style={{
                  width: deviceWidth,
                  marginLeft: -20,
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "300",
                    color: "#777",
                    paddingLeft: -50,
                    textAlign: "center"
                  }}
                >
                  FILTER BY
                </Text>
              </View>
            </View>
            <Grid>
              <Col
                size={1}
                style={{
                  backgroundColor: "#F5F5F6",
                  borderRightColor: "#ddd",
                  borderRightWidth: 1
                }}
              >
                <View>
                  <List
                    contentContainerStyle={{}}
                    dataArray={dataSaleThumb}
                    renderRow={item =>
                      // console.log(item.id);
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ component: item.categories });
                        }}
                      >
                        <View
                          style={{
                            padding: 12,
                            borderBottomColor: "#ddd",
                            borderBottomWidth: 1
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 12,
                              fontWeight: "300",
                              color:
                                this.state.component === item.categories
                                  ? commonColor.brandPrimary
                                  : "#000"
                            }}
                          >
                            {item.categories}
                          </Text>
                        </View>
                      </TouchableOpacity>}
                  />
                </View>
              </Col>
              <Col size={2} style={{ backgroundColor: "#FFFFFF" }}>
                <CommonRefine comp={this.state.component} />
              </Col>
            </Grid>
            <Footer
              style={{
                marginBottom: 0,
                height: 45,
                width: deviceWidth,
                borderTopWidth: 0,
                shadowColor: "#999",
                shadowRadius: 1,
                shadowOpacity: 0.2
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: deviceWidth,
                  alignItems: "center"
                }}
              >
                <Button
                  transparent
                  style={{
                    width: deviceWidth / 2,
                    alignSelf: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{ fontSize: 12, fontWeight: "500", color: "#777" }}
                  >
                    CLEAR ALL
                  </Text>
                </Button>
                <View
                  style={{
                    alignItems: "center",
                    height: 30,
                    width: 1,
                    backgroundColor: "#ddd"
                  }}
                />
                <Button
                  transparent
                  style={{
                    width: deviceWidth / 2,
                    alignSelf: "center",
                    justifyContent: "center"
                  }}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      fontWeight: "500",
                      color: "#7468c5"
                    }}
                  >
                    APPLY
                  </Text>
                </Button>
              </View>
            </Footer>
          </Modal>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({});
export default Refine;
