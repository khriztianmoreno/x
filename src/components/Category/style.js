/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import commonColor from '../../theme/variables/commonColor';

export default {
  bannerHeading: {
    fontSize: 11,
    fontWeight: '300',
    color: '#696d79',
    marginLeft: 0,
    paddingLeft: 0,
  },
  clearAll: {
    fontSize: 12,
    fontWeight: '400',
    color: commonColor.brandDanger,
  },
  contactList: {
    alignItems: 'center',
  },
  contactListItem: {
    color: commonColor.brandPrimary,
    fontSize: 18,
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  listHeading: {
    fontSize: 16,
    color: '#444',
    fontWeight: '600',
  },
  listDropText: {
    fontSize: 14,
    fontWeight: '300',
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  listDropItems: {
    marginLeft: 0,
  },
  listIconHeading: {
    fontWeight: '100',
    paddingTop: 2,
    fontSize: 18,
    color: '#444',
  },
  listIcon: {
    fontWeight: '100',
    paddingTop: 2,
    fontSize: 18,
    color: '#777',
  },
};
