import React, {Component} from "react";
import {
  View,
  H1,
  Text,
  InputGroup,
  Input,
  Icon,
  Header,
  Container,
  Button,
  List,
  ListItem,
  Badge,
  Content,
  Card,
  CardItem,
  Left,
  Right,
  Title,
  Body,
  Grid,
  Col
} from "native-base";
import {
  Image,
  Dimensions,
  Styleheet,
  TouchableOpacity,
  ScrollView,
  Platform
} from "react-native";
import Analytics from 'appcenter-analytics';

import MyFooter from "../CommonComponents/Footer";
import ThemeHeader from "../CommonComponents/Header/index.js";

import Style from "./style.js";

const deviceWidth = Dimensions.get("window").width;


class Category extends Component {
  constructor(props) {
    super(props);
    const { params } = props.navigation.state;

    this.state = {
      listCategories: params ? params.list : [],
      listName: params ? params.name : null,
      categoryDrop: null,
      analyticsEnabled: false
    };
  }

  async componentDidMount() {
    const component = this;

    try {
      const analyticsEnabled = await Analytics.isEnabled();
      component.setState({ analyticsEnabled });
    } catch (error) {
      console.warn("Error", error);
    }
    if (this.state.listCategories.length === 0) {
      this.makeRemoteRequest();
    }
  }

  makeRemoteRequest() {
    fetch('https://inventory.bevyup.com/nordstrom_o1/categories/')
      .then((response) => response.json())
      .then((response) => {
        if (response.statusCode >= 400) {
          console.log('Error')
        } else {
          this.setState({ listCategories: response })
        }
      });
  }

  subCategoriesLoad({ id, name }) {
    fetch(`https://inventory.bevyup.com/nordstrom_o1/categories/${id}`)
      .then((response) => response.json())
      .then((response) => {
        if (response.statusCode >= 400) {
          console.log('Error')
        } else {
          if (response && response.length > 0) {
            this.props.navigation.navigate("CATEGORIES", { list: response, name });
          } else {
            this.props.navigation.navigate("ProductList", { name, id });
          }
        }
      });
  }

  categoryDropdown(id) {
    if (this.state.categoryDrop === id) {
      this.setState({categoryDrop: null});
      this.setState({arrowDirection: "ios-arrow-dropdown-outline"});
      return;
    }
    this.setState({categoryDrop: id});
  }

  render() {
    const navigation = this.props.navigation;

    return (
      <Container>
        <ThemeHeader PageTitle="CATEGORIES" IconRight="search" searchBar="true" navigation={navigation} />
        <Content
          showsVerticalScrollIndicator={false}
          style={{
          marginBottom: 0,
          flex: 1
        }}>
          <Text>
            Analytics enabled: {this.state.analyticsEnabled ? 'yes' : 'no'}
          </Text>
          {
            this.state.listName && (
              <View
                style={{
                  alignItems: "center",
                  height: 50,
                  width: deviceWidth,
                  backgroundColor: "#fff",
                  flexDirection: "row",
                  borderBottomColor: "#ddd",
                  borderBottomWidth: 1
                }}
              >
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", alignItems: "center" }}>
                  <Button transparent>
                    <Icon
                      onPress={() => navigation.goBack()}
                      style={{ padding: 5, marginLeft: -5 }}
                      name="arrow-back"
                    />
                  </Button>
                  <Text style={{ alignItems: "center" }}>
                    { this.state.listName }
                  </Text>
                  <Button transparent>
                  </Button>
                </View>
              </View>
            )
          }
          <Card>
            <CardItem style={{padding: 0}}>
              <List
                removeClippedSubviews={false}
                bounces={false}
                dataArray={this.state.listCategories}
                renderRow={list => <ListItem
                icon
                style={Style.listDropItems}
                onPress={() => this.subCategoriesLoad(list)}>
                <Body>
                  <Text style={Style.listDropText}>
                    {list.name}
                  </Text>
                </Body>
                <Right>
                  <Icon style={Style.listIcon} name="ios-arrow-forward"/>
                </Right>
              </ListItem>}/>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
export default Category;
