import React, {Component} from "react";
import {
  View,
  Image,
  Dimensions,
  StyleSheet,
  Modal,
  TouchableHighlight,
  TouchableOpacity,
  ListView
} from "react-native";
import {
  Container,
  Content,
  Title,
  H1,
  H3,
  H4,
  Text,
  Header,
  Button,
  Icon,
  Card,
  CardItem,
  List,
  ListItem
} from "native-base";

import ThemeHeader from "../CommonComponents/Header";
import ListThumb from "../CommonComponents/ListThumb";
import SortRefine from "../CommonComponents/SortRefine";

import Style from "./style.js";
import MyFooter from "../CommonComponents/Footer";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allProducts: [],
      page: 0,
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    this.makeRemoteRequest(params.id);
  }

  makeRemoteRequest(categoryId) {
    this.setState({ page: this.state.page + 1})
    console.log('AAAA', this.state.page)
    fetch(`https://inventory.bevyup.com/nordstrom_o1/products/${categoryId}?page=${this.state.page}`)
      .then((response) => response.json())
      .then((response) => {
        if (response.statusCode >= 400) {
          console.log('Error')
        } else {
          this.setState({ allProducts: response.products })
        }
      });
  }

  render() {
    const {params} = this.props.navigation.state;
    const navigation = this.props.navigation;

    return (
      <Container>
        <ThemeHeader
          PageTitle="PRODUCT LIST"
          IconRight="search"
          searchBar="true"
          route="product"
          navigation={navigation}/>
        <Content
          style={{ marginBottom: 0, flex: 1 }}
          contentContainerStyle={{ paddingBottom: 10}}
        >
          <View
            style={{
              alignItems: "center",
              height: 50,
              width: deviceWidth,
              backgroundColor: "#fff",
              flexDirection: "row",
              borderBottomColor: "#ddd",
              borderBottomWidth: 1
            }}
          >
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", alignItems: "center" }}>
              <Button transparent>
                <Icon
                  onPress={() => navigation.goBack()}
                  style={{ padding: 5, marginLeft: -5 }}
                  name="arrow-back"
                />
              </Button>
              <Text style={{ alignItems: "center" }}>
                {params.name}
              </Text>
              <Button transparent>
                <Icon
                  onPress={() => navigation.navigate("Filters")}
                  style={{ padding: 5, marginLeft: -5 }}
                  name="ios-options"
                />
              </Button>
              </View>
          </View>

          <List
            bounces={false}
            contentContainerStyle={Style.listThumb}
            dataArray={this.state.allProducts}
            renderRow={item => <ListThumb
            navigation={navigation}
            brand={item.brand}
            price={item.price}
            discount={item.discount}
            discountedPrice={item.sale_price}
            id={item.id}
            description={ item.title.length > 25 ? `${item.title.substring(0, 25)} ...` : item.title }
            imageSource={item.image_link}/>}/>
        </Content>
      </Container>
    );
  }
}
export default ProductList;
