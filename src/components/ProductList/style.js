import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  listThumb: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
});
